import TaskList from "./components/TaskList";
import CreateTask from "./components/CreateTask";
import {useState} from 'react';
import './App.css';

function App() {

  const [textInput, setTextInput] = useState("");
  const [tasks, setTasks] = useState([]);

  const numbers = [1, 2, 3, 4, 5];
  const TaskLi = numbers.map((number) =>
  <li>{number}</li>
);

  return (
    <div className="todo-app">
      <h1>Emres To Do List</h1>
      <CreateTask textInput={textInput} setTextInput={setTextInput} tasks={tasks} setTasks={setTasks}/>
      <TaskList tasks={tasks}/>
    </div>
  );
};

export default App;