const CreateTask = ({setTextInput, textInput, setTasks, tasks}) => {

    const submitTaskHandler = (e) => {
        e.preventDefault()
        setTasks([...tasks, textInput]);
        setTextInput('');
    }

    const userInputHandler = (e) => {
        setTextInput(e.target.value);
    }

    return (
        <div>
            <form className="todo-form" onSubmit={submitTaskHandler}>
                <input type="text"  className="todo-input" value={textInput} onChange={userInputHandler} placeholder="Add your task here" required/>
                <input type="submit" className="bn-1 bn1" value="Submit"/>
            </form>
        </div>
    );
};

export default CreateTask;