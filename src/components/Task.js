const Task = ({tasks}) => {
    return (
        <div className="task-row">
            <h2>{tasks}</h2>
            <div>
                <button className="btn"><i className="fa fa-trash"></i> Delete</button>
            </div>
        </div>
    );
};

export default Task;