import Task from "./Task";

const TaskList = ({tasks}) => {
    return (
        <div className="taskList">
            {tasks.map((task) => (
                <Task tasks={task}/>
            ))}
        </div>
    );
};


export default TaskList;